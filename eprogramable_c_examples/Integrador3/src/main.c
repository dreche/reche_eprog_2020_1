/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdint.h>
#include <stdio.h>

/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/
typedef struct {
	uint8_t port; /*!< GPIO port number */
	uint8_t pin; /*!< GPIO pin number */
	uint8_t dir; /*!< GPIO direction ‘0’ IN;  ‘1’ OUT */
} gpioConf_t;


void Puerto (uint8_t dig_BCD, gpioConf_t * vector)
{
	uint8_t cond1,cond2,cond3,cond4;

	cond1 = (dig_BCD&1);//me quedo SOLO con el b0 y el resto es 0
	dig_BCD>>=1;			//Hago un corrimiento de un lugar

	cond2 = (dig_BCD&1);//me quedo solo con el b1 y el resto es 0
	dig_BCD>>=1;			//Hago un corrimiento de un lugar

	cond3 = (dig_BCD&1);//me quedo solo con el b2 y el resto es 0
	dig_BCD>>=1;			//Hago un corrimiento de un lugar

	cond4 = (dig_BCD&1);//me quedo solo con el b3 y el resto es 0


	/* PUERTO 1.4 */
	if (cond1==1){

		printf("Puerto %d.%d en ALTO \r\n", vector[0].port,vector[0].pin);
	}

	else {

		printf("Puerto %d.%d en BAJO \r\n", vector[0].port,vector[0].pin);
	}

	/* PUERTO 1.5 */
	if (cond2==1){

		printf("Puerto %d.%d en ALTO \r\n", vector[1].port,vector[1].pin);
	}

	else {

		printf("Puerto %d.%d en BAJO \r\n", vector[1].port,vector[1].pin);
	}


	/* PUERTO 1.6 */
	if (cond3==1){

		printf("Puerto %d.%d en ALTO \r\n", vector[2].port,vector[2].pin);

	}

	else {

		printf("Puerto %d.%d en BAJO \r\n", vector[2].port,vector[2].pin);
	}

	/* PUERTO 2.14 */
	if (cond4==1){

			printf("Puerto %d.%d en ALTO \r\n", vector[3].port,vector[3].pin);
	}

	else {

		printf("Puerto %d.%d en BAJO \r\n", vector[3].port,vector[3].pin);
	}


}

int main(void)
{
	gpioConf_t vble_BCD [] = {{1,4,1},{1,5,1},{1,6,1},{2,14,1}};
	uint8_t digito = 14;

	Puerto (digito, vble_BCD);

	return 0;
}

/*==================[end of file]============================================*/

