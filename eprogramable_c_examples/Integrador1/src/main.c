/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
/*A) Realice un función que reciba un puntero a una estructura LED como la que se muestra a
continuación:
struct leds
{
uint8_t n_led; indica el número de led a controlar
uint8_t n_ciclos; indica la cantidad de ciclos de encendido/apagado
uint8_t periodo; indica el tiempo de cada ciclo
uint8_t mode; ON, OFF, TOGGLE
} my_leds;*/

#include "main.h"
#include <stdint.h>
#include <stdio.h>
#include <string.h>


struct leds
{
	uint8_t n_led;    //indica el numero de led a controlar
	uint8_t n_ciclos; //indica la cantidad de ciclos de encendido/apagado
	uint8_t periodo;  //indica el tiempo de cada ciclo
	uint8_t mode;     //ON, OFF, TOGGLE //esto ya lo digo yo (1 para ON, 2 para OFF, 3 para TOOGLE
} my_leds;

//DEFINIR FUNCION
/*==================[inclusions]=============================================*/

void operar_leds(struct leds *led)
{
	uint8_t numled, ciclos, per, modo, i=0;

	numled = led->n_led;
	ciclos = led->n_ciclos;
	per    = led->periodo;
	modo   = led->mode;

	if(modo==1)
	{
		switch(numled) {
			case 1: printf("Encender led 1");
			break;
			case 2: printf("Encender led 2");
			break;
			case 3: printf("Encender led 3");
			break;
			default: printf("Error");
			}
		//printf("Encender led: %d", numled);//trabajar con switch y asignar una frase de salida para cada caso
	}

	if(modo==2)
	{
		switch(numled) {
					case 1: printf("Apagar led 1");
					break;
					case 2: printf("Apagar led 2");
					break;
					case 3: printf("Apagar led 3");
					break;
					default: printf("Error");
					}
	}

	if(modo==3)
	{
		switch(numled) {

					case 1: while (i<ciclos)
						{
						printf("Encender led 1\n");
						printf("Esperar %d", per);
						printf(" segundos \n");
						printf("Apagar led 1\n");
						i++;
						}

					break;
					case 2:  while (i<ciclos)
					{
						printf("Encender led 2\n");
						printf("Esperar %d", per);
						printf(" segundos \n");
						printf("Apagar led 2\n");
						i++;
					}

				break;

					case 3:  while (i<ciclos)
					{
						printf("Encender led 3\n");
						printf("Esperar %d", per);
						printf(" segundos \n");
						printf("Apagar led 3\n");
						i++;
					}

				break;

					//default: puts("Error");
						}
		}

	//printf("Paso por aca");


};

/*==================[macros and definitions]=================================*/



/*==================[internal functions declaration]=========================*/

int main(void)
{
   struct leds *led1;

   led1->n_led = 3;
   led1->n_ciclos = 6;
   led1->periodo = 3;
   led1->mode = 3;

   operar_leds(led1);

	return 0;
}

/*==================[end of file]============================================*/

