/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdint.h>
#include <stdio.h>

/*==================[macros and definitions]=================================*/
/*16.
a. Declare una variable sin signo de 32 bits y cargue el valor 0x01020304. Declare
cuatro variables sin signo de 8 bits y, utilizando máscaras, rotaciones y truncamiento,
cargue cada uno de los bytes de la variable de 32 bits.
b. Realice el mismo ejercicio, utilizando la definición de una “union”.
*/
/*==================[internal functions declaration]=========================*/

int main(void)
{

	uint32_t var1;//genero variable de 32 bits
	var1=0x01020304;//le asigno el valor decimal 16909060

	//printf ("%d ", var1);

	uint8_t aux1;//creo las variables de salida
	uint8_t aux2;
	uint8_t aux3;
	uint8_t aux4;

	aux1 = (uint8_t) var1;//levanto los 8 bits menos significativos
	var1>>=8;//colocar debajo del print			  //desplazo 8 bits el numero principal hacia la derecha
	printf ("%d ", aux1); //imprimo los 8 bits levantados

	aux2 = (uint8_t) var1;
	var1>>=8;
	printf ("%d ", aux2);

	aux3 = (uint8_t) var1;
	var1>>=8;
	printf ("%d ", aux3);

	aux4 = (uint8_t) var1;
	printf("%d ", aux4);
//--------------------------------------------

		struct cada_byte{

			uint8_t byte1;
			uint8_t byte2;
			uint8_t byte3;
			uint8_t byte4;

		}todos_los_bytes;

	todos_los_bytes.byte1=aux1;
	todos_los_bytes.byte2=aux2;
	todos_los_bytes.byte3=aux3;
	todos_los_bytes.byte4=aux4;

	printf("%d ", todos_los_bytes.byte1);
	printf("%d ", todos_los_bytes.byte2);
	printf("%d ", todos_los_bytes.byte3);
	printf("%d ", todos_los_bytes.byte4);


	return 0;
}

/*==================[end of file]============================================*/

