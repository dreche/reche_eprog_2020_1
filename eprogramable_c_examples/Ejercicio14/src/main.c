/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdint.h>
#include <stdio.h>
#include <string.h>


//struct va declarado afuera

/*==================[macros and definitions]=================================*/

/*14. Declare una estructura “alumno”, con los campos “nombre” de 12 caracteres, “apellido” de 20
caracteres y edad.
a. Defina una variable con esa estructura y cargue los campos con sus propios datos.
b. Defina un puntero a esa estructura y cargue los campos con los datos de su
compañero (usando acceso por punteros).*/
/*==================[internal functions declaration]=========================*/

struct clase {
	char nombre[12];
	char apellido[20];
	uint8_t edad;
};

int main(void)
{

	//a------------------------------------------------------
	char name[] = "Diego";
	char surname[] = "Reche";
	uint8_t age = 25;

	struct clase alumno1;

	strcpy(alumno1.nombre, name);
	strcpy(alumno1.apellido, surname);
	alumno1.edad = age;

	printf("Nombre:%s ", alumno1.nombre);
	printf("Apellido:%s ", alumno1.apellido);

	//b--------------------------------------------------------

	struct clase *alumno2;

	char name1[] = "Claudio";
	char surname1[] = "Silva";
	uint8_t age1 = 33;

	strcpy(alumno2->nombre, &name1);
	strcpy(alumno2->apellido, &surname1);
	alumno2->edad = &age1;

	printf("Nombre: %s ",alumno2->nombre);
	printf("Apellido: %s ", alumno2->apellido);





	return 0;
}

/*==================[end of file]============================================*/

