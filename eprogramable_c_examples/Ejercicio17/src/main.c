/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
/*17. Realice un programa que calcule el promedio de los 15 números listados abajo, para ello,
primero realice un diagrama de flujo similar al presentado en el ejercicio 9. (Puede utilizar la
aplicación Draw.io). Para la implementación, utilice el menor tamaño de datos posible:
234 123 111 101 32
116 211 24 214 100
124 222 1 129 9
*/

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdint.h>
#include <stdio.h>

/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/

int main(void)
{
    /*El numero maximo de los que debemos sumar no es mayor a 255, por lo que
	  podemos utilizar variables SIN SIGNO de 8 bits*/

	uint8_t numeros[15] = {234, 123, 111, 101, 32, 116, 211, 24, 214, 100, 124, 222, 1, 129, 9};

	uint8_t i = 0;
	uint16_t valorac = 0; /*La suma de todos los valores es 1751, la variable de menor tamaño utilizable es
						    un entero sin signo de 16 bits*/

	while(i<15)
	{
		valorac += numeros[i];
		i++;
	}

	valorac/=15;

	printf("%d", valorac);
	printf("\n");

//-----------------------------------------EJERCICIO 18-----------------------------------------------------

	i = 0;
	uint8_t numeros2[16];/*Defino un arreglo de 16 elementos*/

	while(i<15)
	{
		numeros2[i] = numeros[i];/*Agrego los 15 elementos de numeros[] en numeros2[]*/
		i++;
	}

	numeros2[15] = 233;/*Completo el arreglo con el 16° elemento*/

	i = 0;
	valorac = 0;

	while(i<16)
	{
		valorac += numeros2[i];
		i++;
	}

	valorac>>=4; /*Realizo una rotación de 4 lugares hacia la derecha,
	               Cada vez que se rota es equivalente a dividir por 2,
	               4 rotaciones equivale a dividir por (2*2*2*2) = 16*/

	printf("%d", valorac);


	return 0;
}

/*==================[end of file]============================================*/

