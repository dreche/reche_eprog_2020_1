########################################################################
#Cátedra: Electrónica Programable
#FIUNER
#2018
#Autor/es:
#JMreta - jmreta@ingenieria.uner.edu.ar
#
#Revisión:
########################################################################

# Ruta del Proyecto
####Ejemplo 1: Hola Mundo
##Por el primero, se debe usar Makefile1
#PROYECTO_ACTIVO = 1_hola_mundo
#NOMBRE_EJECUTABLE = hola_mundo.exe

####Ejemplo 2: Control Mundo
#PROYECTO_ACTIVO = 2_control_mundo
#NOMBRE_EJECUTABLE = control_mundo.exe

####Ejemplo 3: Promedio
#PROYECTO_ACTIVO = 3_promedio_1
#NOMBRE_EJECUTABLE = promedio.exe

#PROYECTO_ACTIVO = 5_menu
#NOMBRE_EJECUTABLE = menu.exe

#PROYECTO_ACTIVO = Ejercicio1
#NOMBRE_EJECUTABLE = Ejercicio1.exe

#PROYECTO_ACTIVO = Ejercicio2
#NOMBRE_EJECUTABLE = Ejercicio2.exe

#PROYECTO_ACTIVO = Ejercicio3
#NOMBRE_EJECUTABLE = Ejercicio3.exe

#PROYECTO_ACTIVO = Ejercicio7
#NOMBRE_EJECUTABLE = Ejercicio7.exe

#PROYECTO_ACTIVO = Ejercicio9
#NOMBRE_EJECUTABLE = Ejercicio9.exe

#PROYECTO_ACTIVO = Ejercicio12
#NOMBRE_EJECUTABLE = Ejercicio12.exe

#PROYECTO_ACTIVO = Ejercicio13
#NOMBRE_EJECUTABLE = Ejercicio13.exe

#PROYECTO_ACTIVO = Ejercicio14
#NOMBRE_EJECUTABLE = Ejercicio14.exe

#PROYECTO_ACTIVO = Ejercicio16
#NOMBRE_EJECUTABLE = Ejercicio16.exe

#PROYECTO_ACTIVO = Ejercicio17
#NOMBRE_EJECUTABLE = Ejercicio17.exe

#PROYECTO_ACTIVO = Integrador1
#NOMBRE_EJECUTABLE = Integrador1.exe

#PROYECTO_ACTIVO = Integrador2
#NOMBRE_EJECUTABLE = Integrador2.exe

PROYECTO_ACTIVO = Integrador3
NOMBRE_EJECUTABLE = Integrador3.exe