/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
/*C) Escriba una función que reciba un dato de 32 bits, la cantidad de dígitos de salida y un puntero a
un arreglo donde se almacene los n dígitos. La función deberá convertir el dato recibido a BCD,
guardando cada uno de los dígitos de salida en el arreglo pasado como puntero.
int8_t BinaryToBcd (uint32_t data, uint8_t digits, uint8_t * bcd_number )
{
}
*/
#include "main.h"
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>


//La funcion no deberia devolver un Void? Si la carga de elementos al arreglo no necesita devolver nada--> puede utilizarse para devolver un codigo de error
void BinaryToBcd (uint32_t data, uint8_t digits, uint8_t *bcd_number )
{
	uint8_t i = 0;
	//uint8_t arreglo[8];
	uint8_t var1, var2=0;

	var1=((1<<0)|(1<<1)|(1<<2)|(1<<3));//xxxx1111
	var2|=var1;//00001111


	while(i<digits)
	{
		bcd_number[digits-1-i] = data%10;//Cargo el resto de la división por 10 (Ejemplo: el resto de 1034/10 = 4)
		//printf("%d ", bcd_number[i]);
		data/=10;//pasa de 1034 a 103, 10 y 1... Es decir, por cada vez que pasa por aqui pierde una cifra
		i++;
	}

}

/*==================[inclusions]=============================================*/


/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/

int main(void)
{


	uint8_t array[8];//instancio un arreglo de 8 elementos (cada digito son 4 bits -> 8 elementos*4 bits = 32 bits del dato)

	uint32_t dato = 958; // en binario 0100 0000 1010 = 4, 0, 10

	uint8_t n_dig = 8;

	BinaryToBcd (dato, n_dig, &array); // Devuelve el arreglo que muestra en orden desde el bit - sign, al + sign

	uint8_t i = 0;

	while(i<n_dig)
	{
		printf("%d ", array[i]);
		i++;
	}


	return 0;
}

/*==================[end of file]============================================*/

