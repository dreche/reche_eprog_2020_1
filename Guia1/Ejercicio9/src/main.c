/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdint.h>
#include <stdio.h>
/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/

int main(void)
{
    uint32_t var1; //defino la variable de 32 bits
    uint32_t A;

    printf("%d ", var1);//veo qué tiene la variable var1
    var1>>=3;			//desplazo 3 bits hacia la izquierda y me quedo con el bit 4 en la posicion 1
    printf("%d ", var1);//veo que tiene el bit 4, es decir, el actual bit 1


    var1&=1; //limpio el resto de los bits y me quedo SOLO con el primer bit (bit 4, actual bit 1)
    printf("%d ", var1);//veo qué quedó


    if(var1==0)//Si el bit 4, actual bit 1, es 0
    {
    	A = 0;
    	printf("%d ", A);
    }

    else//Si el bit 4, actual bit 1, no es 0
    {
    	A = 0xaa;
    	printf("%d ", A);
    }
	return 0;
}

/*==================[end of file]============================================*/

