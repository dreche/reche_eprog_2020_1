/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
/*Modifique la aplicación 1_blinking_switch de manera de hacer titilar los leds verde,
amarillo y rojo al mantener presionada las teclas 2, 3 y 4 correspondientemente.
También se debe poder hacer titilar los leds individuales del led RGB, para ello se
deberá mantener presionada la tecla 1 junto con la tecla 2, 3 o 4
correspondientemente.
En el siguiente video se puede observar el funcionamiento deseado del sistema:
LINK*/

/*==================[inclusions]=============================================*/
#include "blinking_switch.h"       /* <= own header */

#include "systemclock.h"
#include "led.h"
#include "switch.h"

/*==================[macros and definitions]=================================*/
#define COUNT_DELAY 3000000
/*==================[internal data definition]===============================*/

void Delay(void)
{
	uint32_t i;

	for(i=COUNT_DELAY; i!=0; i--)
	{
		   asm  ("nop");/*Qué hace esta funcion?*//*RTA: No hace nada pero indica que ejecuta en ASSEMBLER*/
	}
}

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{
	uint8_t teclas;
	SystemClockInit();
	LedsInit();/*Inicializa los LEDs (1, 2 y 3, R, G y B) en salida*/
	SwitchesInit();/*Inicializa los Switchs como entrada*/
	//LedOn(LED_3);/*Enciende el led 3*/
    while(1)/*Se ejecuta constantemente*/
    {/*En la EDU CIAA el LED1 es AMARILLO, el LED2 es ROJO y el LED3 es VERDE*/

    	teclas  = SwitchesRead();/*Se carga en "teclas" el (valor del) switch que fue accionado (1 para s1, 2 para s2, 4 para s3, 8 para s4)*/
    	switch(teclas)
    	{
    		case SWITCH_2:/*Si se presiona el switch 2, se enciende el led3 y luego de un período de tiempo se apaga*/
    			LedOn(LED_3);
    			Delay();
    			LedOff(LED_3);
    			Delay();
    		break;

    		case SWITCH_3:/*Si se presiona el switch 3, se enciende el led1 y luego de un período de tiempo se apaga*/
    			LedOn(LED_1);
    			Delay();
    			LedOff(LED_1);
    			Delay();
    		break;

    		case SWITCH_4:/*Si se presiona el switch 4, se enciende el led2 y luego de un período de tiempo se apaga*/
    		   	LedOn(LED_2);
    		 	Delay();
    		   	LedOff(LED_2);
    		    Delay();
    		break;
    		/*En caso de que teclas=0011*/
    		case (SWITCH_1 | SWITCH_2):/*Si se presiona el s1 y el s2, se enciende el ledRGB_R y luego de un período de tiempo se apaga*/
    		    LedOn(LED_RGB_R);
    		    Delay();
    		    LedOff(LED_RGB_R);
    		    Delay();
    		break;
    		/*En caso de que teclas=0101*/
    		case (SWITCH_1 | SWITCH_3):/*Si se presiona el s1 y el s3, se enciende el ledRGB_G y luego de un período de tiempo se apaga*/
    		    LedOn(LED_RGB_G);
    		    Delay();
    		    LedOff(LED_RGB_G);
    		    Delay();
    		break;
    		/*En caso de que teclas=1001*/
    		case (SWITCH_1 | SWITCH_4):/*Si se presiona el s1 y el s4, se enciende el ledRGB_B y luego de un período de tiempo se apaga*/
    		    LedOn(LED_RGB_B);
    		    Delay();
    		    LedOff(LED_RGB_B);
    		    Delay();
    		break;
    	}
	}

    
}

/*==================[end of file]============================================*/

