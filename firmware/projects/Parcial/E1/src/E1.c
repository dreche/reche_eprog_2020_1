/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
/*Aplicación para uso de sensor de distancia*/

/*==================[inclusions]=============================================*/
#include "../inc/E1.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "hc_sr4.h"
#include "stdint.h"
#include "stdio.h"
#include "delay.h"
#include "bool.h"
#include "timer.h"
#include "uart.h"


/*==================[macros and definitions]=================================*/
/*void medir_distancia(void);*/

float pi = 3.14;
uint16_t dcm;
uint16_t volumen;
uint16_t altura;

/*==================[internal data definition]===============================*/

timer_config my_timer = {TIMER_A,100,&medir_distancia};/*Defino mi timer*/


/*==================[internal functions declaration]=========================*/

void medir_distancia(void)
{
	 dcm = HcSr04ReadDistanceCentimeters();

	 altura = 12 - dcm; /*Calculo la altura del agua, es decir, desde la base a la superficie*/

	 volumen = pi*2*2*altura;/*calculo el volumen ocupado por el agua*/

	 UartSendString(SERIAL_PORT_PC, UartItoa(volumen,10));/*transformo el valor a ASCII*/
	 UartSendString(SERIAL_PORT_PC, " ");
	 UartSendString(SERIAL_PORT_PC, "cm3");
	 UartSendString(SERIAL_PORT_PC, "\r\n");


}



void SysInit(void)
{
	SystemClockInit();
	TimerInit(&my_timer);/*Inicializo mi timer*/
	TimerStart(TIMER_A);

}


/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{

	serial_config *serial1;/*Defino mi puerto serie*/
	serial1->port = SERIAL_PORT_PC;
	serial1->baud_rate = 115200;
	serial1->pSerial = NULL;
	SysInit();

	UartInit(serial1);/*Inicializo mi puerto serie*/

	bool vconfirm1, vconfirm2;

	vconfirm1 = HcSr04Init(T_FIL2,T_FIL3);/*Inicializo mi sensor de distancia con el pin T_FIL2 como el pin de echo y el pin T_FIL3 como el de trigger*/

	while(1)
	{

	}

	vconfirm2 = HcSr04Deinit();

}



/*==================[end of file]============================================*/


