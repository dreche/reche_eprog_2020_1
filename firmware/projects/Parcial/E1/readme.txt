A la placa se debe conectar el sensor de distancia HcSr04.

CONEXIONADO: 
El pin de ECHO deberá conectarse al pin de la placa T_FIL2, mientras que
el pin de TRIGGER deberá conectarse al pin de la plata T_FIL3.

El dispositivo deberá colocarse en la parte superior del recipiente (vaso) del cual se desea
conocer el volumen de agua contenida.

El pin GND deberá conectarse a un punto GND de la placa.

El pin VCC deberá conectarse al punto +5v de la placa.
