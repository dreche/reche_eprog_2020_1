/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
/*Aplicación para uso de sensor de distancia*/

/*==================[inclusions]=============================================*/
#include "../inc/E2.h"       /* <= own header */
#include "systemclock.h"
#include "stdint.h"
#include "stdio.h"
#include "delay.h"
#include "bool.h"
#include "timer.h"
#include "uart.h"
#include "analog_io.h"


/*==================[macros and definitions]=================================*/

uint8_t cont1 = 0;
uint16_t inputvalue;
uint16_t valor_promedio;
uint16_t datos_leidos[10];

/*==================[internal data definition]===============================*/

timer_config my_timer = {TIMER_A,100,&Iniciar_conversion};/*Defino mi timer*/
analog_input_config my_input = {CH2, AINPUTS_SINGLE_READ, &leer_entrada1};/*Defino mi analog input*/

/*==================[internal functions declaration]=========================*/

void Iniciar_conversion(void)/*iniciar conversion*/
{

	AnalogStartConvertion();

}

uint16_t Find_promedio(uint16_t *datos)
{
	uint16_t aux = 0;
	uint8_t contador = 0;

	while(contador<10)
	{
		aux = aux + datos[contador];
		contador++;
	}

	aux = aux/10;
	return(aux);
}

void leer_entrada1(void)
{
	AnalogInputRead(CH2, &inputvalue);/*Cargo en la direccion de memoria de inputvalue el valor de lectura de CH2*/

	if(cont1<10)
	{
		datos_leidos[cont1] = inputvalue;
		cont1++;
	}

	if(cont1>=10)
	{
		cont1 = 0;
		valor_promedio = Find_promedio(&datos_leidos);

		UartSendString(SERIAL_PORT_PC, UartItoa(valor_promedio,10));/*transformo el valor a ASCII*/
		UartSendString(SERIAL_PORT_PC, "\r\n");
	}



}


void SysInit(void)
{
	SystemClockInit();
	TimerInit(&my_timer);/*Inicializo mi timer*/
	TimerStart(TIMER_A);
	AnalogInputInit(&my_input);/*Inicializo mi analog input*/
}


/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{

	serial_config *serial1;/*Defino mi puerto serie*/
	serial1->port = SERIAL_PORT_PC;
	serial1->baud_rate = 115200;
	serial1->pSerial = NULL;
	SysInit();

	UartInit(serial1);/*Inicializo mi puerto serie*/


	while(1)
	{

	}

}



/*==================[end of file]============================================*/


