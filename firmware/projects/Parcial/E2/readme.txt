Para esta aplicación se debe conectar un potenciometro a la placa.
El mismo debe tener una resistencia soldada al pin del centro para evitar la circulación
de corrientes inadecuadas que puedan causar problemas en el microcontrolador.

El pin correspondiente al "maximo" (visto de frente a la perilla del potenciometro, el que 
se encuentra a la derecha), debe estar conectado al pin VDDA de la placa.
El pin correspondiente al "minimo" (visto de frente a la perilla del potenciometro, el que
se encuentra a la izquierda), debe estar conectado a pin GNDA de la placa.

El pin central del potenciometro (resistencia mediante) deberá estar conectado al pin del
conversor A/D identificado como CH2.