/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
/*Aplicación para uso de sensor de distancia*/

/*==================[inclusions]=============================================*/
#include "../inc/Osciloscopio.h"       /* <= own header */

#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "hc_sr4.h"
#include "stdint.h"
#include "stdio.h"
#include "delay.h"
#include "bool.h"
#include "timer.h"
#include "uart.h"
#include "analog_io.h"


/*==================[macros and definitions]=================================*/
void leer_entrada(void);
void leer_entrada1(void);

#define BUFFER_SIZE 256
bool flag = false;
uint8_t cont = 0;

const char ecg[BUFFER_SIZE]={
		17,17,17,17,17,17,17,17,17,17,17,18,18,18,17,17,17,17,17,17,17,18,18,18,18,18,18,18,17,17,16,16,16,16,17,17,18,18,18,17,17,17,17,
		18,18,19,21,22,24,25,26,27,28,29,31,32,33,34,34,35,37,38,37,34,29,24,19,15,14,15,16,17,17,17,16,15,14,13,13,13,13,13,13,13,12,12,
		10,6,2,3,15,43,88,145,199,237,252,242,211,167,117,70,35,16,14,22,32,38,37,32,27,24,24,26,27,28,28,27,28,28,30,31,31,31,32,33,34,36,
		38,39,40,41,42,43,45,47,49,51,53,55,57,60,62,65,68,71,75,79,83,87,92,97,101,106,111,116,121,125,129,133,136,138,139,140,140,139,137,
		133,129,123,117,109,101,92,84,77,70,64,58,52,47,42,39,36,34,31,30,28,27,26,25,25,25,25,25,25,25,25,24,24,24,24,25,25,25,25,25,25,25,
		24,24,24,24,24,24,24,24,23,23,22,22,21,21,21,20,20,20,20,20,19,19,18,18,18,19,19,19,19,18,17,17,18,18,18,18,18,18,18,18,17,17,17,17,
		17,17,17,

};
/*==================[internal data definition]===============================*/

timer_config my_timer = {TIMER_A,2,&leer_entrada};/*Defino mi timer*/
analog_input_config my_input = {CH1, AINPUTS_SINGLE_READ, &leer_entrada1};/*Defino mi analog input*/

/*==================[internal functions declaration]=========================*/

void leer_entrada(void)/*iniciar conversion*/
{
		AnalogStartConvertion();

		if (flag == true)
		{
			if(cont<256)
			{
				AnalogOutputWrite(ecg[cont]);
				cont++;
			}

			if(cont>=256)
			{
				cont = 0;
			}
		}

		flag=!flag;
}

void leer_entrada1(void)
{
	uint16_t inputvalue;
	AnalogInputRead(CH1, &inputvalue);/*Cargo en la direccion de memoria de inputvalue el valor de lectura de CH1*/

	UartSendString(SERIAL_PORT_PC, UartItoa(inputvalue,10));/*transformo el valor a ASCII*/
	UartSendString(SERIAL_PORT_PC, "\r");
}

void SysInit(void)
{
	SystemClockInit();
	TimerInit(&my_timer);/*Inicializo mi timer*/
	TimerStart(TIMER_A);
	AnalogInputInit(&my_input);/*Inicializo mi analog input*/
	AnalogOutputInit();

}


/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{

	serial_config *serial1;/*Defino mi puerto serie*/
	serial1->port = SERIAL_PORT_PC;
	serial1->baud_rate = 115200;
	serial1->pSerial = NULL;
	SysInit();

	UartInit(serial1);/*Inicializo mi puerto serie*/


	while(1)
	{

	}

}



/*==================[end of file]============================================*/


