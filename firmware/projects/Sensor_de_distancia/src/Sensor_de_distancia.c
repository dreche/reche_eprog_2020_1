/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
/*Aplicación para uso de sensor de distancia*/

/*==================[inclusions]=============================================*/
#include "../inc/Sensor_de_distancia.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "hc_sr4.h"
#include "stdint.h"
#include "stdio.h"
#include "delay.h"
#include "bool.h"


/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/
bool medir = false;
bool congelar = false;

/*==================[internal functions declaration]=========================*/
void medir_distancia()
{

	int16_t dcm = HcSr04ReadDistanceCentimeters();

	if(dcm<=10)/*SI LA DISTANCIA ES MENOR A 10CM*/
	{
		LedOn(LED_RGB_B);
		LedOff(LED_1);
		LedOff(LED_2);
		LedOff(LED_3);
	}

	if((dcm>10)&&(dcm<=20)) /*SI LA DISTANCIA ESTÁ ENTRE 10 Y 20CM*/
	{
		LedOn(LED_RGB_B);
		LedOn(LED_1);
		LedOff(LED_2);
		LedOff(LED_3);
	}

	if((dcm>20)&&(dcm<30))	/*SI LA DISTANCIA ESTÁ ENTRE 20 Y 30CM*/
	{
		LedOn(LED_RGB_B);
		LedOn(LED_1);
		LedOn(LED_2);
		LedOff(LED_3);
	}

	if(dcm>30)				/*SI LA DISTANCIA ES MAYOR A 30CM*/
	{
		LedOn(LED_RGB_B);
		LedOn(LED_1);
		LedOn(LED_2);
		LedOn(LED_3);
	}
}

void Tecla_1()
{
	medir =! medir;
}

void Tecla_2()
{
	congelar =! congelar;
}
/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{

cff	SystemClockInit();
	LedsInit();/*Inicializa los LEDs (1, 2 y 3, R, G y B) en salida*/
	SwitchesInit();/*Inicializa los Switchs como entrada*/
	bool confirm1,confirm2;

	//bool medir;      /*BANDERA DE MEDICION*/
	//bool congelar;	 /*BANDERA DE HOLD*/

	//medir = true;
	//congelar = false;

	confirm1 = HcSr04Init(T_FIL2,T_FIL3); /*El pin T_FIL2 es el pin de echo y el pin T_FIL3 es de trigger*/


	SwitchActivInt(SWITCH_1, Tecla_1);
	SwitchActivInt(SWITCH_2, Tecla_2);

	while(1)
	{

		if((medir==true)&&(congelar==false))
		{
			medir_distancia();
		}

		if((medir==true)&&(congelar==true))
		{
			medir_distancia();
			while((congelar==tru}e)&&(medir==true))
			{
				DelayMs(10);
			}
		}

		if((medir==false)&&(congelar==false))
		{
			LedOff(LED_RGB_B);
			LedOff(LED_1);
			LedOff(LED_2);
			LedOff(LED_3);

		}

		if((medir==false)&&(congelar==true))
		{
			LedOff(LED_RGB_B);
			LedOff(LED_1);
			LedOff(LED_2);
			LedOff(LED_3);
		}
	}

    confirm2 = HcSr04Deinit();

}


/*No comprendo la diferencia entre una interrupción y ejecutar una función, si en sí, ambas son subrutinas*/
/*==================[end of file]============================================*/


