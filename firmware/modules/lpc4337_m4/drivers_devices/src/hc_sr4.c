/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
/*Modifique la aplicación 1_blinking_switch de manera de hacer titilar los leds verde,
amarillo y rojo al mantener presionada las teclas 2, 3 y 4 correspondientemente.
También se debe poder hacer titilar los leds individuales del led RGB, para ello se
deberá mantener presionada la tecla 1 junto con la tecla 2, 3 o 4
correspondientemente.
En el siguiente video se puede observar el funcionamiento deseado del sistema:
LINK*/

/*==================[inclusions]=============================================*/
#include "../inc/hc_sr4.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include <stdint.h>
#include <stdio.h>



typedef struct
{
	gpio_t PIN_ECHO; /*Pin de ECHO*/
	gpio_t PIN_TRIGGER; /*Pin de TRIGGER*/

} SensorHcSr04;
 /*const*/

SensorHcSr04 S1;/*Defino un SensorHcSr04 en el cual se cargan el valor de los pines*/


/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/



bool HcSr04Init(gpio_t echo, gpio_t trigger)
{

	bool regreso;
	S1.PIN_ECHO=echo;/*Guardo en memoria el valor del pin ECHO*/
	S1.PIN_TRIGGER=trigger;/*Guardo en memoria el valor del pin TRIGGER*/

	/*Falta definir la instancia de verificación de selección de pines, no sé como comparar elementos
	 * del tipo gpio*/

	GPIOInit(echo, GPIO_INPUT);
	GPIOInit(trigger, GPIO_OUTPUT);
	GPIOOff(trigger);/*Inicializo el pin trigger arranque en bajo*/

	regreso = true;/*Si la instancia de verificación fue positiva*/
	return (regreso);

}

bool HcSr04Deinit()
{
	GPIODeinit();/*No sé por qué solicita los pines de echo y trigger si la función
				  GPIODeinit() no los requiere*/
	bool regreso = true;

	return(regreso);

}

int16_t HcSr04ReadDistanceCentimeters(void)
{
	bool estado;
	uint32_t dilei = 10;/*si son constantes, lo defino como const o define*/
	uint32_t dilei1 = 1;
	uint16_t contador=0;
	uint16_t regreso;

	GPIOOn(S1.PIN_TRIGGER); /*Enciendo el pin de trigger*/
	DelayUs(dilei);		    /*Espero 10uS*/
	GPIOOff(S1.PIN_TRIGGER);/*Apago    el pin de trigger*/

	estado = GPIORead (S1.PIN_ECHO);/*Veo en qué estado está el pin ECHO*/

	if(estado==0)
	{
		while(estado==0)
		{
			estado = GPIORead (S1.PIN_ECHO);/*Esta linea y la anterior refrescan el valor de la entrada ECHO cada 1 uSeg*/
		}
	}

	if (estado==1)
	{
		while (estado==1)
		{
			contador++;/*Cuenta los uSeg que la entrada ECHO está en alto*/
			DelayUs(dilei1);
			estado = GPIORead (S1.PIN_ECHO);/*Esta linea y la anterior refrescan el valor de la entrada ECHO cada 1 uSeg*/
		}
	}

	regreso=(contador/24);/*Segun la formula, este cálculo me devuelve la distancia en CM*/


	return(regreso);

}

int16_t HcSr04ReadDistanceInches(void)
{
	bool estado;
	uint32_t dilei = 10;
	uint32_t dilei1 = 1;
	uint16_t contador=0;
	uint16_t regreso;

	GPIOOn(S1.PIN_TRIGGER); /*Enciendo el pin de trigger*/
	DelayUs(dilei);		    /*Espero 10uS*/
	GPIOOff(S1.PIN_TRIGGER);/*Apago    el pin de trigger*/

	estado = GPIORead (S1.PIN_ECHO);/*Veo en qué estado está el pin ECHO*/

	if(estado==0)
	{
		while(estado==0)
		{
			DelayUs(dilei1);
			estado = GPIORead (S1.PIN_ECHO);/*Esta linea y la anterior refrescan el valor de la entrada ECHO cada 1 uSeg*/
		}
	}

	if (estado==1)
	{
		while (estado==1)
		{
			contador++;/*Cuenta los uSeg que la entrada ECHO está en alto*/
			DelayUs(dilei1);
			estado = GPIORead (S1.PIN_ECHO);/*Esta linea y la anterior refrescan el valor de la entrada ECHO cada 1 uSeg*/
		}
	}

	regreso=(contador/148);/*Segun la formula, este cálculo me devuelve la distancia en In*/


	return(regreso);

}



/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/



/*==================[end of file]============================================*/

